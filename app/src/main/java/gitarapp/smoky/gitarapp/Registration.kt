package gitarapp.smoky.gitarapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.smoky.gitarapp.R
import kotlinx.android.synthetic.main.activity_registration.buttonRegister
import kotlinx.android.synthetic.main.activity_registration.editTextRegEmail
import kotlinx.android.synthetic.main.activity_registration.editTextRegUsername
import kotlinx.android.synthetic.main.activity_registration.textViewExistingUser
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class Registration : AppCompatActivity() {

    private lateinit var database: DatabaseReference
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var showHidePasswordButton: ImageButton
    private lateinit var showHidePasswordButton2: ImageButton
    private lateinit var editTextRegPassword: EditText
    private lateinit var editTextRegConfirmPassword: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        firebaseAuth = FirebaseAuth.getInstance()

        showHidePasswordButton = findViewById(R.id.showHidePasswordButton)
        showHidePasswordButton2 = findViewById(R.id.showHidePasswordButton2)
        editTextRegPassword = findViewById(R.id.editTextRegPassword)
        editTextRegConfirmPassword = findViewById(R.id.editTextRegConfirmPassword)

        showHidePasswordButton.setOnClickListener {
            if ( editTextRegPassword.transformationMethod is PasswordTransformationMethod) {
                // Trenutno je prikazana skrivena lozinka, promijeni način prikaza u običan tekst
                editTextRegPassword.transformationMethod = null
                showHidePasswordButton.setImageResource(R.drawable.ic_password_visibility_on)
            } else {
                // Trenutno je prikazan običan tekst, promijeni način prikaza u skrivenu lozinku
                editTextRegPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                showHidePasswordButton.setImageResource(R.drawable.ic_password_visibility_off)
            }

            // Osvježi tekst kako bi se prikazala nova transformacija
            editTextRegPassword.setSelection( editTextRegPassword.text?.length ?: 0)
        }

        showHidePasswordButton2.setOnClickListener {
            if (  editTextRegConfirmPassword.transformationMethod is PasswordTransformationMethod) {
                // Trenutno je prikazana skrivena lozinka, promijeni način prikaza u običan tekst
                editTextRegConfirmPassword.transformationMethod = null
                showHidePasswordButton2.setImageResource(R.drawable.ic_password_visibility_on)
            } else {
                // Trenutno je prikazan običan tekst, promijeni način prikaza u skrivenu lozinku
                editTextRegConfirmPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                showHidePasswordButton2.setImageResource(R.drawable.ic_password_visibility_off)
            }

            // Osvježi tekst kako bi se prikazala nova transformacija
            editTextRegConfirmPassword.setSelection(  editTextRegConfirmPassword.text?.length ?: 0)
        }

        buttonRegister.setOnClickListener{
            val username = editTextRegUsername.text.toString()
            val email = editTextRegEmail.text.toString()
            val password = editTextRegPassword.text.toString()
            val confirmPassword = editTextRegConfirmPassword.text.toString()

            val user = User(username, email)

            if (username.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty() && confirmPassword.isNotEmpty()){
                if (password == confirmPassword){
                    firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener{
                        if (it.isSuccessful){
                            database = FirebaseDatabase.getInstance().getReference("Korisnici")
                            database.child(username).setValue(user).addOnSuccessListener {
                                Toast.makeText(this,"Uspješno spremljeno", Toast.LENGTH_SHORT).show()
                            }.addOnFailureListener{
                                Toast.makeText(this,"Greška", Toast.LENGTH_SHORT).show()
                            }
                            val intent = Intent(this, Login::class.java)
                            startActivity(intent)
                        } else {
                            Toast.makeText(this, it.exception.toString(), Toast.LENGTH_SHORT).show()
                        }
                    }
                } else{
                    Toast.makeText(this, "Zaporka se ne podudara", Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, "Polja ne mogu biti prazna", Toast.LENGTH_SHORT).show()
            }
        }
        textViewExistingUser.setOnClickListener{
            val loginIntent = Intent(this, Login::class.java)
            startActivity(loginIntent)
        }
    }

    private fun updateUserInfo(){
        val timestamp = System.currentTimeMillis()
        val uid = firebaseAuth.uid

        val ref = FirebaseDatabase.getInstance().getReference("Korisnici")
        ref.child(uid!!)
            .setValue(uid)

    }
}