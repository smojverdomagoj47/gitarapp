package gitarapp.smoky.gitarapp

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.values
import com.google.gson.Gson
import com.smoky.gitarapp.R
import gitarapp.smoky.gitarapp.ui.calculateFrequency
import kotlinx.android.synthetic.main.fragment_pjesme.button

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [Pjesme.newInstance] factory method to
 * create an instance of this fragment.
 */
class Pjesme : Fragment() {
    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var buttonLogout: Button
    private lateinit var emailTextView: TextView
    private lateinit var usernameTextView: TextView

    private lateinit var ime1TextView: TextView
    private lateinit var ime2TextView: TextView
    private lateinit var ime3TextView: TextView
    private lateinit var ime4TextView: TextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pjesme, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firebaseAuth = FirebaseAuth.getInstance()
        ime1TextView = view.findViewById(R.id.textView4)
        ime2TextView = view.findViewById(R.id.textView3)
        ime3TextView = view.findViewById(R.id.textView5)
        ime4TextView = view.findViewById(R.id.textView6)

        val user = FirebaseAuth.getInstance().currentUser?.email
        val database = FirebaseDatabase.getInstance().reference
        val korisniciRef = database.child("Korisnici")
        val query = korisniciRef.orderByChild("Ime")
        var username = "idk"

        query.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (snapshot in dataSnapshot.children) {
                    username = snapshot.child("username").value.toString()
                    break
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
            }
        })

        button.setOnClickListener{
            val databaseReference = FirebaseDatabase.getInstance().getReference("Korisnici/$username")
            val novaPjesma = "1"
            databaseReference.child("pjesme").setValue(novaPjesma)
                .addOnSuccessListener {
                    Toast.makeText(context, "Yay", Toast.LENGTH_SHORT).show()
                }
                .addOnFailureListener {
                    Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
                }
        }




        val databaseReference = FirebaseDatabase.getInstance().getReference("Pjesme/1")
            databaseReference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val prva = dataSnapshot.child("Ime").value.toString()
                    ime1TextView.text = prva
                }
                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
                }
         })

        val databaseReference2 = FirebaseDatabase.getInstance().getReference("Pjesme/2")
        databaseReference2.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val druga = dataSnapshot.child("Ime").value.toString()
                ime2TextView.text = druga
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
            }
        })

        val databaseReference3 = FirebaseDatabase.getInstance().getReference("Pjesme/3")
        databaseReference3.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val treca = dataSnapshot.child("Ime").value.toString()
                ime3TextView.text = treca
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
            }
        })

        val databaseReference4 = FirebaseDatabase.getInstance().getReference("Pjesme/4")
        databaseReference4.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val cetvrta = dataSnapshot.child("Ime").value.toString()
                ime4TextView.text = cetvrta
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
            }
        })
        }
}