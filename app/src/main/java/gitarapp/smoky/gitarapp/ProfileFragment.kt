package gitarapp.smoky.gitarapp

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.smoky.gitarapp.R

class ProfileFragment : Fragment() {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var buttonLogout: Button
    private lateinit var emailTextView: TextView
    private lateinit var usernameTextView: TextView

    private lateinit var weightTextView: TextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_profile, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        firebaseAuth = FirebaseAuth.getInstance()
        buttonLogout = view.findViewById(R.id.buttonLogout)
        emailTextView = view.findViewById(R.id.emailTextView)
        usernameTextView = view.findViewById(R.id.usernameTextView)
        weightTextView = view.findViewById(R.id.weightTextView)



        buttonLogout.setOnClickListener{
            firebaseAuth.signOut()
            val loginIntent = Intent(requireContext(), Login::class.java)
            startActivity(loginIntent)
            requireActivity().finish()
        }


        val currentUser = FirebaseAuth.getInstance().currentUser
        val currentUserEmail = currentUser?.email

        var pjesma = "1"

        if (!currentUserEmail.isNullOrEmpty()) {
            val database = FirebaseDatabase.getInstance().reference
            val korisniciRef = database.child("Korisnici")

            val query = korisniciRef.orderByChild("Ime")

            query.addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    for (snapshot in dataSnapshot.children) {
                        val username = snapshot.child("username").value.toString()
                        val email = snapshot.child("email").value.toString()
                        val pjesme = snapshot.child("pjesme").value.toString()
                        pjesma = pjesme


                        usernameTextView.text = username
                        emailTextView.text = email

                        // Prekini petlju nakon pronalaska odgovarajućeg korisnika
                        break
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
                }
            })
        } else {
            Toast.makeText(context, "Korisnik nije ulogiran", Toast.LENGTH_SHORT).show()
        }

        val databaseReference = FirebaseDatabase.getInstance().getReference("Pjesme/$pjesma")
        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val prva = dataSnapshot.child("Ime").value.toString()
                weightTextView.text = prva
            }
            override fun onCancelled(databaseError: DatabaseError) {
                Toast.makeText(context, "Greška prilikom dohvaćanja podataka", Toast.LENGTH_SHORT).show()
            }
        })

    }
}
